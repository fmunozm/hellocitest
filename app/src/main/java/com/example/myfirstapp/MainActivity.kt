package com.example.myfirstapp

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), MainPresenter.View {

    private val presenter = MainPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.attackView(this)

        presenter.doSomething("HelloWorld")
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showOk(result: String) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
    }

    override fun showError() {
        Toast.makeText(this, "Error Occurred", Toast.LENGTH_SHORT).show()
    }
}