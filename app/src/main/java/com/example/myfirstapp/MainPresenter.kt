package com.example.myfirstapp

import android.view.View

class MainPresenter {
    private var view : View? = null

    fun attackView(view : View) {
        this.view = view
    }

    fun detachView() {
        this.view = null
    }

    fun doSomething(data: String) {
        if (data.isNotEmpty())
            view?.showOk(data)
        else
            view?.showError()
    }

    interface View {
        fun showOk(result: String)
        fun showError()
    }
}