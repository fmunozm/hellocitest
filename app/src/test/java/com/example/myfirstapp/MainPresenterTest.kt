package com.example.myfirstapp

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import org.junit.Before
import org.junit.Test

class MainPresenterTest {

    private lateinit var presenter: MainPresenter
    private lateinit var view: MainPresenter.View

    @Before
    fun setUp() {
        presenter = MainPresenter()
        view = mock()
        presenter.attackView(view)
    }

    @Test
    fun doSomething_withEmpty_Test() {
        presenter.doSomething("")
        verify(view).showError()
    }

    @Test
    fun doSomething_withText_Test() {
        presenter.doSomething("NotEmpty")
        verify(view).showOk("NotEmpty")
    }

    @Test
    fun presenter_detachView_Test() {
        presenter.detachView()
        verifyNoMoreInteractions(view)
    }
}