package com.example.myfirstapp

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class ExampleInstrumentedTest {

    @Test
    fun useAppContext() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        assertEquals("com.example.myfirstapp", context.packageName)
    }
}